# FreeBSD Pantheon development repo

Development is continuing [here](https://codeberg.org/olivierd/freebsd-ports-elementary).

This is the [FreeBSD Ports](https://svnweb.freebsd.org/ports/head/) collection in order to make this desktop environment usable.

It is an **early state** (very unstable).
