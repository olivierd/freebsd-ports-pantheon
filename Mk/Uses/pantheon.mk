# $FreeBSD$
#
# Provide support for Pantheon related ports.
#
# Feature:      pantheon
# Usage:        USES= pantheon
# Valid ARGS:   does not require args
#
# Variable which can be set by the port:
# USE_PANTHEON  List of components
#
# MAINTAINER: ports@FreeBSD.org

.if !defined(_INCLUDE_USES_PANTHEON_MK)
_INCLUDE_USES_PANTHEON_MK=  yes

.if !empty(pantheon_ARGS)
IGNORE= Incorrect USES+= pantheon:${pantheon_ARGS} takes no arguments
.endif

DIST_SUBDIR=    pantheon

# Available Pantheon components are:
_USE_PANTHEON_ALL=  gala granite plank switchboard wingpanel

gala_LIB_DEPENDS=       libgala.so:x11-wm/gala
gala_RUN_DEPENDS=       gala:x11-wm/gala
gala_USE_PANTHEON_REQ=  granite plank

granite_LIB_DEPENDS=    libgranite.so:x11-toolkits/granite

plank_LIB_DEPENDS=  libplank.so:x11/plank
plank_RUN_DEPENDS=  plank:x11/plank

switchboard_LIB_DEPENDS=	libswitchboard-2.0.so:sysutils/switchboard
switchboard_USE_PANTHEON_REQ=	granite

wingpanel_LIB_DEPENDS=	libwingpanel-2.0.so:x11/wingpanel
wingpanel_RUN_DEPENDS=	wingpanel:x11/wingpanel
wingpanel_USE_PANTHEON_REQ=	gala

.if defined(USE_PANTHEON)

# First, expand all USE_PANTHEON_REQ recursively.
.for comp in ${_USE_PANTHEON_ALL}
. for subcomp in ${${comp}_USE_PANTHEON_REQ}
${comp}_USE_PANTHEON_REQ+=  ${${subcomp}_USE_PANTHEON_REQ}
. endfor
.endfor

# Then, use already expanded USE_PANTHEON_REQ to expand USE_PANTHEON.
.for comp in ${USE_PANTHEON}
. if empty(_USE_PANTHEON_ALL:M${comp})
IGNORE= cannot install: Unknown component ${comp}
. else
_USE_PANTHEON+= ${${comp}_USE_PANTHEON_REQ} ${comp}
. endif
.endfor

# Remove duplicate components
USE_PANTHEON=   ${_USE_PANTHEON:O:u}

.for comp in ${USE_PANTHEON}
. if defined(${comp}_BUILD_DEPENDS)
BUILD_DEPENDS+= ${${comp}_BUILD_DEPENDS}
. endif

. if defined(${comp}_LIB_DEPENDS)
LIB_DEPENDS+= ${${comp}_LIB_DEPENDS}
. endif

. if defined(${comp}_RUN_DEPENDS)
RUN_DEPENDS+= ${${comp}_RUN_DEPENDS}
. endif
.endfor

.endif # end of defined(USE_PANTHEON)

.endif # end of !defined(_INCLUDE_USES_PANTHEON_MK)

.if defined(_POSTMKINCLUDED) && !defined(_INCLUDE_USES_PANTHEON_POST_MK)
_INCLUDE_USES_PANTHEON_POST_MK= yes
.endif
