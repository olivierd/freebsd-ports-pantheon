# Created by: Koop Mast <kwm@FreeBSD.org>
# $FreeBSD$

PORTNAME=	zeitgeist
PORTVERSION=	1.0.2
CATEGORIES=	sysutils
PKGNAMESUFFIX=	1

MAINTAINER=	desktop@FreeBSD.org
COMMENT=	Event logging framework

LICENSE=	GPLv2 LGPL21 LGPL3+
LICENSE_COMB=	multi

CONFLICTS_INSTALL=	zeitgeist

BUILD_DEPENDS=	valac:lang/vala \
		${PYTHON_PKGNAMEPREFIX}rdflib>=4.1.1:textproc/py-rdflib@${PY_FLAVOR}

USES=		autoreconf compiler:gcc-c++11-lib gettext-tools gmake \
		gnome python:3.5+,build pkgconfig libtool sqlite
GNU_CONFIGURE=	yes
USE_GCC=	yes
USE_GNOME=	glib20 introspection:build
USE_LDCONFIG=	yes
INSTALL_TARGET=	install-strip

OPTIONS_DEFINE=	DATAHUB FTS
OPTIONS_DEFAULT=	DATAHUB
OPTIONS_SUB=	yes

DATAHUB_DESC=		User logger activities
DATAHUB_LIB_DEPENDS=	libjson-glib-1.0.so:devel/json-glib
DATAHUB_USE=		gnome=cairo,gdkpixbuf2,gtk30
DATAHUB_CONFIGURE_ENABLE=	datahub

FTS_DESC=		Full-text search support
FTS_LIB_DEPENDS=	libxapian.so:databases/xapian-core
FTS_CONFIGURE_ENABLE=	fts

CONFIGURE_ARGS+=--without-dee-icu \
	--enable-explain-queries \
	--disable-telepathy \
	--disable-downloads-monitor

USE_GITLAB=	yes
GL_SITE=	https://gitlab.freedesktop.org
GL_COMMIT=	f6394278664b19210823d27e9c04d363f38bd33d

post-patch:
	@${REINPLACE_CMD} -e '/python/d ' \
		${WRKSRC}/Makefile.am

.include <bsd.port.mk>
