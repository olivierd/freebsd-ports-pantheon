--- src/Application.vala.orig	2020-03-30 21:01:11 UTC
+++ src/Application.vala
@@ -459,22 +459,10 @@ namespace Switchboard {
             // list of names taken from GCC's shell/cc-panel-loader.c
             switch (gcc_name) {
                 case "background": return "pantheon-desktop";
-                case "bluetooth": return "network-pantheon-bluetooth";
                 case "color": return "hardware-gcc-color";
-                case "datetime": return "system-pantheon-datetime";
                 case "display": return "system-pantheon-display";
-                case "info": return "system-pantheon-about";
                 case "keyboard": return "hardware-pantheon-keyboard";
-                case "network": return "pantheon-network";
-                case "power": return "system-pantheon-power";
-                case "printers": return "pantheon-printers";
-                case "privacy": return "pantheon-security-privacy";
-                case "region": return "system-pantheon-locale";
-                case "sharing": return "pantheon-sharing";
                 case "sound": return "hardware-gcc-sound";
-                case "universal-access": return "pantheon-accessibility";
-                case "user-accounts": return "system-pantheon-useraccounts";
-                case "wacom": return "hardware-gcc-wacom";
                 case "notifications": return "personal-pantheon-notifications";
 
                 // not available on our system
