--- src/Widgets/IconGroup.vala.orig	2020-04-03 18:45:09 UTC
+++ src/Widgets/IconGroup.vala
@@ -580,12 +580,12 @@ namespace Gala {
             if (destination is WorkspaceInsertThumb) {
                 get_parent ().remove_child (this);
 
+#if HAS_MUTTER332
                 unowned WorkspaceInsertThumb inserter = (WorkspaceInsertThumb) destination;
-#if HAS_MUTTER330
                 unowned Meta.WorkspaceManager manager = workspace.get_display ().get_workspace_manager ();
+
+                // reorder_workspace () not available with mutter 3.30!
                 manager.reorder_workspace (workspace, inserter.workspace_index);
-#else
-                workspace.get_screen ().reorder_workspace (workspace, inserter.workspace_index);
 #endif
 
                 restore_group ();
