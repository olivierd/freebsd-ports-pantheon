--- src/InternalUtils.vala.orig	2020-04-03 18:45:09 UTC
+++ src/InternalUtils.vala
@@ -118,7 +118,7 @@ namespace Gala {
             x11display.set_stage_input_region (xregion);
 #else
             var xregion = X.Fixes.create_region (display.get_x11_display ().get_xdisplay (), rects);
-            Util.set_stage_input_region (display, xregion);
+            display.set_stage_input_region (xregion);
 #endif
         }
 #else
