--- src/Widgets/WindowSwitcher.vala.orig	2020-04-03 18:45:09 UTC
+++ src/Widgets/WindowSwitcher.vala
@@ -101,7 +101,7 @@ namespace Gala {
             add_child (dock);
 
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.connect (update_actors);
+            Meta.MonitorManager.@get ().monitors_changed_internal.connect (update_actors);
 #else
             wm.get_screen ().monitors_changed.connect (update_actors);
 #endif
@@ -115,7 +115,7 @@ namespace Gala {
 
 
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.disconnect (update_actors);
+            Meta.MonitorManager.@get ().monitors_changed_internal.disconnect (update_actors);
 #else
             wm.get_screen ().monitors_changed.disconnect (update_actors);
 #endif
