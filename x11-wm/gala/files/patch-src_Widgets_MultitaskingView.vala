--- src/Widgets/MultitaskingView.vala.orig	2020-04-03 18:45:09 UTC
+++ src/Widgets/MultitaskingView.vala
@@ -109,7 +109,7 @@ namespace Gala {
             window_containers_monitors = new List<MonitorClone> ();
             update_monitors ();
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.connect (update_monitors);
+            Meta.MonitorManager.@get ().monitors_changed_internal.connect (update_monitors);
 #else
             screen.monitors_changed.connect (update_monitors);
 #endif
