--- src/Background/BackgroundSource.vala.orig	2020-04-03 18:45:09 UTC
+++ src/Background/BackgroundSource.vala
@@ -44,7 +44,7 @@ namespace Gala {
             backgrounds = new Gee.HashMap<int,Background> ();
 
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.connect (monitors_changed);
+            Meta.MonitorManager.@get ().monitors_changed_internal.connect (monitors_changed);
 #else
             screen.monitors_changed.connect (monitors_changed);
 #endif
@@ -114,7 +114,7 @@ namespace Gala {
 
         public void destroy () {
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.disconnect (monitors_changed);
+            Meta.MonitorManager.@get ().monitors_changed_internal.disconnect (monitors_changed);
 #else
             screen.monitors_changed.disconnect (monitors_changed);
 #endif
