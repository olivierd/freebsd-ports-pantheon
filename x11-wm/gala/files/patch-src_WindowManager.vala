--- src/WindowManager.vala.orig	2020-04-03 18:45:09 UTC
+++ src/WindowManager.vala
@@ -296,7 +296,7 @@ namespace Gala {
             /*hot corner, getting enum values from GraniteServicesSettings did not work, so we use GSettings directly*/
             configure_hotcorners ();
 #if HAS_MUTTER330
-            Meta.MonitorManager.@get ().monitors_changed.connect (configure_hotcorners);
+            Meta.MonitorManager.@get ().monitors_changed_internal.connect (configure_hotcorners);
 #else
             screen.monitors_changed.connect (configure_hotcorners);
 #endif
