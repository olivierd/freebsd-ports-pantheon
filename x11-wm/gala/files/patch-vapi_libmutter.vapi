--- vapi/libmutter.vapi.orig	2020-04-03 18:45:09 UTC
+++ vapi/libmutter.vapi
@@ -436,7 +436,7 @@ namespace Meta {
 		public string get_pad_action_label (Clutter.InputDevice pad, Meta.PadActionType action_type, uint action_number);
 #if HAS_MUTTER330
 		public int get_primary_monitor ();
-#if !HAS_MUTTER334
+#if HAS_MUTTER334
 		public unowned Meta.Selection get_selection ();
 #endif
 		public void get_size (out int width, out int height);
