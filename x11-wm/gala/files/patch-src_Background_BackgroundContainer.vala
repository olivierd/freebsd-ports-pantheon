--- src/Background/BackgroundContainer.vala.orig	2020-04-03 18:45:09 UTC
+++ src/Background/BackgroundContainer.vala
@@ -27,13 +27,13 @@ namespace Gala {
         }
 
         construct {
-            Meta.MonitorManager.@get ().monitors_changed.connect (update);
+            Meta.MonitorManager.@get ().monitors_changed_internal.connect (update);
 
             update ();
         }
 
         ~BackgroundContainer () {
-            Meta.MonitorManager.@get ().monitors_changed.disconnect (update);
+            Meta.MonitorManager.@get ().monitors_changed_internal.disconnect (update);
         }
 #else
         public Meta.Screen screen { get; construct; }
