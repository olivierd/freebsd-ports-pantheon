--- src/Services/UserManager.vala.orig	2020-04-01 23:50:50 UTC
+++ src/Services/UserManager.vala
@@ -41,12 +41,12 @@ public class Session.Services.UserManager : Object {
     public Wingpanel.Widgets.Separator users_separator { get; construct; }
 
     private const uint GUEST_USER_UID = 999;
-    private const uint NOBODY_USER_UID = 65534;
-    private const uint RESERVED_UID_RANGE_END = 1000;
+    // According to pw.conf(5)
+    private const uint MAX_USER_UID = 32000;
+    private const uint MIN_USER_UID = 1000;
 
     private const string DM_DBUS_ID = "org.freedesktop.DisplayManager";
-    private const string LOGIN_IFACE = "org.freedesktop.login1";
-    private const string LOGIN_PATH = "/org/freedesktop/login1";
+    private const string CK_IFACE = "org.freedesktop.ConsoleKit";
 
     private Act.UserManager manager;
     private Gee.HashMap<uint, Widgets.Userbox>? user_boxes;
@@ -56,36 +56,44 @@ public class Session.Services.UserManager : Object {
 
     static construct {
         try {
-            login_proxy = Bus.get_proxy_sync (BusType.SYSTEM, LOGIN_IFACE, LOGIN_PATH, DBusProxyFlags.NONE);
+            login_proxy = Bus.get_proxy_sync (BusType.SYSTEM,
+                                              CK_IFACE,
+                                              "/org/freedesktop/ConsoleKit/Manager",
+                                              DBusProxyFlags.NONE);
         } catch (IOError e) {
             critical ("Failed to create login1 dbus proxy: %s", e.message);
         }
     }
 
     public static UserState get_user_state (uint32 uuid) {
+        UserInterface? user_interface = null;
+        GLib.ObjectPath ssid;
+        string state;
+        uint32 user_id;
+
         if (login_proxy == null) {
             return UserState.OFFLINE;
         }
 
         try {
-            UserInfo[] users = login_proxy.list_users ();
-            if (users == null) {
+            login_proxy.get_current_session (out ssid);
+
+            user_interface = Bus.get_proxy_sync (BusType.SYSTEM,
+                                                 CK_IFACE, ssid);
+            if (user_interface != null) {
+                user_interface.get_session_state (out state);
+                user_interface.get_unix_user (out user_id);
+            }
+            else {
                 return UserState.OFFLINE;
             }
 
-            foreach (UserInfo user in users) {
-                if (user.uid == uuid) {
-                    if (user.user_object == null) {
-                        return UserState.OFFLINE;
-                    }
-                    UserInterface? user_interface = Bus.get_proxy_sync (BusType.SYSTEM, LOGIN_IFACE, user.user_object, DBusProxyFlags.NONE);
-                    if (user_interface == null) {
-                        return UserState.OFFLINE;
-                    }
-                    return UserState.to_enum (user_interface.state);
-                }
+            if (user_id == uuid) {
+                return UserState.to_enum (state);
             }
-
+            else {
+                return UserState.OFFLINE;
+            }
         } catch (GLib.Error e) {
             critical ("Failed to get user state: %s", e.message);
         }
@@ -94,23 +102,7 @@ public class Session.Services.UserManager : Object {
     }
 
     public static UserState get_guest_state () {
-        if (login_proxy == null) {
-            return UserState.OFFLINE;
-        }
-
-        try {
-            UserInfo[] users = login_proxy.list_users ();
-            foreach (UserInfo user in users) {
-                var state = get_user_state (user.uid);
-                if (user.user_name.has_prefix ("guest-")
-                    && state == UserState.ACTIVE) {
-                    return UserState.ACTIVE;
-                }
-            }
-        } catch (GLib.Error e) {
-            critical ("Failed to get Guest state: %s", e.message);
-        }
-
+        // Currently disable
         return UserState.OFFLINE;
     }
 
@@ -165,7 +157,7 @@ public class Session.Services.UserManager : Object {
     private void add_user (Act.User? user) {
         // Don't add any of the system reserved users
         var uid = user.get_uid ();
-        if (uid < RESERVED_UID_RANGE_END || uid == NOBODY_USER_UID || user_boxes.has_key (uid)) {
+        if (uid < MIN_USER_UID || uid >= MAX_USER_UID) {
             return;
         }
 
