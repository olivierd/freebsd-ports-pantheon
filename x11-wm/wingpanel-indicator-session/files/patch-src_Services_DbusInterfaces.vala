--- src/Services/DbusInterfaces.vala.orig	2020-04-01 23:50:50 UTC
+++ src/Services/DbusInterfaces.vala
@@ -17,35 +17,32 @@
  * Boston, MA 02110-1301 USA
  */
 
-struct UserInfo {
-    uint32 uid;
-    string user_name;
-    ObjectPath? user_object;
-}
-
 /* Power and system control */
 [DBus (name = "org.freedesktop.ScreenSaver")]
 interface LockInterface : Object {
     public abstract void lock () throws GLib.Error;
 }
 
-[DBus (name = "org.freedesktop.login1.User")]
+[DBus (name = "org.freedesktop.ConsoleKit.Manager")]
 interface LogoutInterface : Object {
-    public abstract void terminate () throws GLib.Error;
+    public abstract void open_session (out string cookie) throws GLib.Error;
+    public abstract void close_session (string cookie,
+                                        out bool result) throws GLib.Error;
 }
 
-[DBus (name = "org.freedesktop.login1.Manager")]
+[DBus (name = "org.freedesktop.ConsoleKit.Manager")]
 interface SystemInterface : Object {
     public abstract void suspend (bool interactive) throws GLib.Error;
     public abstract void reboot (bool interactive) throws GLib.Error;
     public abstract void power_off (bool interactive) throws GLib.Error;
 
-    public abstract UserInfo[] list_users () throws GLib.Error;
+    public abstract void get_current_session (out GLib.ObjectPath ssid) throws GLib.Error;
 }
 
-[DBus (name = "org.freedesktop.login1.User")]
+[DBus (name = "org.freedesktop.ConsoleKit.Session")]
 interface UserInterface : Object {
-    public abstract string state { owned get; }
+    public abstract void get_session_state (out string type) throws GLib.Error;
+    public abstract void get_unix_user (out uint32 uid) throws GLib.Error;
 }
 
 [DBus (name = "org.freedesktop.DisplayManager.Seat")]
