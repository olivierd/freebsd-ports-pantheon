--- src/Indicator.vala.orig	2020-04-01 23:50:50 UTC
+++ src/Indicator.vala
@@ -22,14 +22,18 @@ public class Session.Indicator : Wingpanel.Indicator {
     private const string KEYBINDING_SCHEMA = "org.gnome.settings-daemon.plugins.media-keys";
 
     private SystemInterface suspend_interface;
+    private LogoutInterface? logout_interface = null;
     private LockInterface lock_interface;
     private SeatInterface seat_interface;
 
+    private string cookie = null;
+
     private Wingpanel.IndicatorManager.ServerType server_type;
     private Wingpanel.Widgets.OverlayIcon indicator_icon;
 
     private Gtk.ModelButton lock_screen;
     private Gtk.ModelButton suspend;
+    private Gtk.ModelButton log_out;
 
     private Session.Services.UserManager manager;
     private Widgets.EndSessionDialog? current_dialog = null;
@@ -49,10 +53,19 @@ public class Session.Indicator : Wingpanel.Indicator {
         EndSessionDialogServer.get_default ().show_dialog.connect ((type) => show_dialog ((Widgets.EndSessionDialogType)type));
     }
 
-    static construct {
+    construct {
         if (SettingsSchemaSource.get_default ().lookup (KEYBINDING_SCHEMA, true) != null) {
             keybinding_settings = new GLib.Settings (KEYBINDING_SCHEMA);
         }
+        try {
+            logout_interface = Bus.get_proxy_sync (BusType.SYSTEM,
+                                                   "org.freedesktop.ConsoleKit",
+                                                   "/org/freedesktop/ConsoleKit/Manager");
+            if (logout_interface != null) {
+                logout_interface.open_session (out cookie);
+            }
+        } catch (GLib.Error e) {
+        }
     }
 
     public override Gtk.Widget get_display_widget () {
@@ -73,6 +86,8 @@ public class Session.Indicator : Wingpanel.Indicator {
     }
 
     public override Gtk.Widget? get_widget () {
+        bool result = false; // response in order to close session
+
         if (main_grid == null) {
             init_interfaces ();
 
@@ -84,9 +99,12 @@ public class Session.Indicator : Wingpanel.Indicator {
 
             var log_out_grid = new Granite.AccelLabel (_("Log Out…"));
 
-            var log_out = new Gtk.ModelButton ();
+            log_out = new Gtk.ModelButton ();
             log_out.get_child ().destroy ();
             log_out.add (log_out_grid);
+            if (cookie == null) {
+                log_out.set_sensitive (false);
+            }
 
             var lock_screen_grid = new Granite.AccelLabel (_("Lock"));
 
@@ -152,7 +170,16 @@ public class Session.Indicator : Wingpanel.Indicator {
                 }
             });
 
-            log_out.clicked.connect (() => show_dialog (Widgets.EndSessionDialogType.LOGOUT));
+            log_out.clicked.connect (() => {
+                try {
+                    logout_interface.close_session (cookie, out result);
+                    if (result) {
+                        show_dialog (Widgets.EndSessionDialogType.LOGOUT);
+                    }
+                } catch (GLib.Error e) {
+                    stderr.printf ("%s\n", e.message);
+                }
+            });
 
             lock_screen.clicked.connect (() => {
                 close ();
@@ -170,7 +197,9 @@ public class Session.Indicator : Wingpanel.Indicator {
 
     private void init_interfaces () {
         try {
-            suspend_interface = Bus.get_proxy_sync (BusType.SYSTEM, "org.freedesktop.login1", "/org/freedesktop/login1");
+            suspend_interface = Bus.get_proxy_sync (BusType.SYSTEM,
+                                                    "org.freedesktop.ConsoleKit",
+                                                    "/org/freedesktop/ConsoleKit/Manager");
         } catch (IOError e) {
             warning ("Unable to connect to suspend interface: %s", e.message);
             suspend.set_sensitive (false);
