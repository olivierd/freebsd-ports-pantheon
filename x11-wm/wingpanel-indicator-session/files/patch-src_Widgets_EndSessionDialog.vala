--- src/Widgets/EndSessionDialog.vala.orig	2020-04-01 23:50:50 UTC
+++ src/Widgets/EndSessionDialog.vala
@@ -29,7 +29,6 @@ public enum Session.Widgets.EndSessionDialogType {
 }
 
 public class Session.Widgets.EndSessionDialog : Gtk.Window {
-    private static LogoutInterface? logout_interface;
     private static SystemInterface? system_interface;
 
     public EndSessionDialogType dialog_type { get; construct; }
@@ -42,13 +41,13 @@ public class Session.Widgets.EndSessionDialog : Gtk.Wi
         var server = EndSessionDialogServer.get_default ();
 
         try {
-            if (dialog_type == Session.Widgets.EndSessionDialogType.LOGOUT && logout_interface == null) {
-                logout_interface = Bus.get_proxy_sync (BusType.SYSTEM, "org.freedesktop.login1", "/org/freedesktop/login1/user/self");
-            } else if (system_interface == null) {
-                system_interface = Bus.get_proxy_sync (BusType.SYSTEM, "org.freedesktop.login1", "/org/freedesktop/login1");
+            if (dialog_type == Session.Widgets.EndSessionDialogType.LOGOUT && system_interface == null) {
+                system_interface = Bus.get_proxy_sync (BusType.SYSTEM,
+                                                       "org.freedesktop.ConsoleKit",
+                                                       "/org/freedesktop/ConsoleKit/Manager");
             }
         } catch (GLib.Error e) {
-            critical ("Unable to connect to login1: %s", e.message);
+            critical ("%s", e.message);
         }
 
         string icon_name, heading_text, button_text, content_text;
@@ -107,7 +106,7 @@ public class Session.Widgets.EndSessionDialog : Gtk.Wi
             confirm_restart.clicked.connect (() => {
                 try {
                     server.confirmed_reboot ();
-                    system_interface.reboot (false);
+                    system_interface.reboot (true);
                 } catch (GLib.Error e) {
                     warning ("Unable to reboot: %s", e.message);
                 }
@@ -172,16 +171,9 @@ public class Session.Widgets.EndSessionDialog : Gtk.Wi
             if (dialog_type == EndSessionDialogType.RESTART || dialog_type == EndSessionDialogType.SHUTDOWN) {
                 try {
                     server.confirmed_shutdown ();
-                    system_interface.power_off (false);
+                    system_interface.power_off (true);
                 } catch (GLib.Error e) {
                     warning ("Unable to shutdown: %s", e.message);
-                }
-            } else {
-                try {
-                    server.confirmed_logout ();
-                    logout_interface.terminate ();
-                } catch (GLib.Error e) {
-                    warning ("Unable to logout: %s", e.message);
                 }
             }
 
