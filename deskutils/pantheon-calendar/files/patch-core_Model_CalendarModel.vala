--- core/Model/CalendarModel.vala.orig	2019-11-27 19:39:03 UTC
+++ core/Model/CalendarModel.vala
@@ -68,7 +68,7 @@ public class Maya.Model.CalendarModel : Object {
     }
 
     private CalendarModel () {
-        int week_start = Posix.NLTime.FIRST_WEEKDAY.to_string ().data[0];
+        int week_start = 2;
         if (week_start >= 1 && week_start <= 7) {
             week_starts_on = (GLib.DateWeekday) (week_start - 1);
         }
